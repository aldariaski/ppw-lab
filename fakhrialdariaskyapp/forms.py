from django import forms
from .models import MataKuliah

class FormMatkul(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = "__all__"
