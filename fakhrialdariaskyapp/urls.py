from django.urls import path

from . import views

app_name = 'fakhrialdariaskyapp'

urlpatterns = [
    path('', views.index, name='home'),
    path('home/', views.index, name='index'),
    path('profile/', views.biodata, name='index'),


    path("matkuls/add",views.matkul_add,name="matkul_add"),
    path("matkuls/<int:id>/read",views.matkul_read,name="matkul_read"),
    path("matkuls/<int:id>/delete",views.matkul_delete,name="matkul_delete"),
    path('matkuls/', views.matkul_index, name='matkul_index'),

]
