from django.shortcuts import render, redirect
#from django.http import HttpResponse
from datetime import datetime, date
from .models import MataKuliah
from .forms import FormMatkul
from django.contrib import messages

# Enter your name here
mhs_name = 'Yusuf Fakhri Aldrian' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
curr_month = int(datetime.now().strftime("%m"))
curr_day = int(datetime.now().strftime("%d"))
angkatan = 2019
birth_date = date(2002, 12, 28) #TODO Implement this, format (Year, Month, Date)
birth_day_complete = str(birth_date.day) + " " + birth_date.strftime("%B") + " " + str(birth_date.year)
hobby = "Menonton film dan membaca"
npm = 1906351096 # TODO Implement this
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year, birth_date.month, birth_date.day), 'npm': npm, 'hobby': hobby, 'angkatan': angkatan, "birthday": birth_day_complete}
    return render(request, 'main/index_page.html', response)
    
def biodata(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year, birth_date.month, birth_date.day), 'npm': npm, 'hobby': hobby, 'angkatan': angkatan, "birthday": birth_day_complete}
    return render(request, 'main/index_lab1.html', response)

def calculate_age(birth_year, birth_month, birth_day):
    if birth_year > curr_year:
        return 0
    elif curr_month < birth_month:
        return curr_year - birth_year - 1
    elif curr_month == birth_month:
        if curr_day < birth_day:
            return curr_year - birth_year - 1
        return curr_year - birth_year
    else:
        return curr_year - birth_year

def matkul_index(request):
    context = {
        "mata_kuliah_list" : MataKuliah.objects.all().order_by("-id"),
    }
    return render(request, "main/index_story5.html", context)

def matkul_add(request):
    if request.method == "POST":
        matkul_form = FormMatkul(request.POST)
        if matkul_form.is_valid():
            matkul_form.save()
            messages.add_message(request, messages.SUCCESS, "Matkul '{}' Ditambahkan".format(request.POST["nama"]))
        return redirect("/matkuls")
    return render(request,"main/matkul_add.html")

def matkul_read(request,id):
    matkul = MataKuliah.objects.get(id=id)
    context = {
        "mata_kuliah" : matkul,
    }
    return render(request,"main/matkul_read.html", context)

def matkul_delete(request,id):
    if request.method == "POST":
        mata_kuliah = MataKuliah.objects.get(id=id)
        mata_kuliah.delete()
        messages.add_message(request, messages.SUCCESS, "Matkul '{}' Dihapuskan".format(mata_kuliah.nama))
    return redirect("/matkuls")

    
