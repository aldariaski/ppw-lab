from django.db import models
from django.shortcuts import reverse

semesters = [
    ("2019/2020 Gasal","2019/2020 Gasal"),
    ("2019/2020 Genap","2019/2020 Genap"),
    ("2020/2021 Gasal","2020/2021 Gasal"),
    ("2020/2021 Genap","2020/2021 Genap"),
    ("2021/2022 Gasal","2021/2022 Gasal"),
    ("2021/2022 Genap","2021/2022 Genap"),
    ("2022/2023 Gasal","2022/2023 Gasal"),
    ("2022/2023 Genap","2022/2023 Genap")
]

class MataKuliah(models.Model):
    nama = models.TextField(max_length = 100)
    dosen = models.TextField(max_length = 70)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField(max_length = 1000)
    semester = models.TextField(choices = semesters)
    ruangan = models.TextField(max_length = 100)

    def __str__(self):
        return "{} ({})".format(self.nama,self.dosen)

    def get_absolute_url(self):
        return reverse("fakhrialdariaskyapp:matkul_read",args=[self.id])

