from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MataKuliah',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.TextField(max_length=100)),
                ('dosen', models.TextField(max_length=70)),
                ('jumlah_sks', models.IntegerField()),
                ('deskripsi', models.TextField(max_length=1000)),
                ('semester', models.TextField(choices=[('2019/2020 Gasal', '2019/2020 Gasal'), ('2019/2020 Genap', '2019/2020 Genap'), ('2020/2021 Gasal', '2020/2021 Gasal'), ('2020/2021 Genap', '2020/2021 Genap'), ('2021/2022 Gasal', '2021/2022 Gasal'), ('2021/2022 Genap', '2021/2022 Genap'), ('2022/2023 Gasal', '2022/2023 Gasal'), ('2022/2023 Genap', '2022/2023 Genap')])),
                ('ruangan', models.TextField(max_length=100)),
            ],
        ),
    ]
